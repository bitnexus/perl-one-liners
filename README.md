# Perl one-liners

## Synopsis

These are a handful of one-liners that were egregious enough for me to save or share in Slack or Matrix. The reaction
from the comedy troupe crowd is usually a mix of awe, disgust and eventually anger. This kind of motivation is
not necessary, but does make writing one-liners that much more fun.

That said, there are almost always better ways to do it than this, but they are FAST and dirty ways to get one-off
stuff out of the way if you understand the syntax and just want to get it done. I just make it work and move on. Just
don't do this if you need to commit the code anywhere or use it often; write a program instead, and use Perl if you
can!

I'm just putting these up for now, will add explanations when I've got time.

## The one-liners

### Get the number of non-terminated nodes in an EKS cluster

My cluster specifically, which always matches the regexes used. At least for now.

```
kubectl get nodes | perl -ane 'next unless $.>1; print "$F[0]: "; ($l)=grep /^Non/, qx(kubectl describe node $F[0]); printf("%s\n", $l=~/(\d+)/)'
ip-192-168-10-255.ec2.internal: 13
ip-192-168-19-236.ec2.internal: 14
ip-192-168-35-8.ec2.internal: 18
```

### Build a config file using a list of URI references read from a file

Yeah this is ugly and yeah there are definitely better ways to do it, but it took about 3 minutes to generate hundreds
of configs, and it really pissed off my coworkers; just the fact that the ability to do it exists and that I did it.
Perl is awesome.

```
perl -F'/' -lne 'open $fh, ">", "$F[0]-$F[2]-$F[-1].conf";print $fh qq({\n  "server": "https://$F[1].$F[2].$F[0].$F[3].privatedomain.io",\n  "bootstrap.servers": "something.something.confluent.cloud:9092"\n})' configs
```

Configs would have looked like

```
datascience/smartapp/dev/aws
frontend/prettyapp/prod/aws
product/shinyapp/uat/gcp
```

A generated config would have looked like

```
{
  "server": "https://smartapp.dev.datascience.gcp.privatedomain.io",
  "bootstrap.servers": "something.something.confluent.cloud:9092"
}
```

### Really stupid/lazy way to export environment variables for your AWS credentials

When the idea of doing something so mundane by hand is incomprehensibly tedious

```
$(grep -A2 [^-]nonprod ~/.aws/credentials | grep -v '^\[' | perl -lane 'print "export \U$F[0]\E=$F[2]"')
```

### Count the number of commits from your teammates

Find out which members on your team are most responsible for the atrocious monolith that somehow works and provides
crucial functionality for the business. Can't hate on the results, but you can hate on the code.

```
$ git blame src/something.py | perl -lane '$res{$F[2]}++;END{printf "%s: %s\n", s/[()]//gr, $res{$_} for sort {$res{$b} <=> $res{$a}} keys %res}'
Joe: 2478
John: 696
Dave: 248
Calvin: 246
PlatformBot: 64
Newguy: 6
```

### Convert a config file with passwords stored in one format to another format

```
perl -F= -le '$F[0] =~ s/\./_/g;$F[0] = uc($F[0]);print "$F[0]=$F[1]"' <<<"something.with.a.password=thepassword"
SOMETHING_WITH_A_PASSWORD=thepassword
```

### Take a file with a bunch of names and tell me which names don't have an existing config YAML

```
perl -F, -lne 'print if ! -f sprintf("%s.yml", $F[0])' Ids\ to\ Remove.csv
```
